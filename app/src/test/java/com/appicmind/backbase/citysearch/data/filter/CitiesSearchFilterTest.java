package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;


/**
 * @author marcocoenradie
 * @since 17-10-2019
 */
public class CitiesSearchFilterTest {

    // Normally I would mock these objects, but since the assignment states that the only 3rd party
    // libs allowed are GSON and Jackson I'm using actual objects
    private City Alabama        = new City("Alabama","US");
    private City Albuquerque    = new City("Albuquerque","US");
    private City Anaheim        = new City("Anaheim", "US");
    private City Arizona        = new City("Arizona", "US");
    private City SydneyAU       = new City("Sydney", "AU");
    private City SydneyUS       = new City("Sydney", "US");
    private final List<City> mCities = new ArrayList<>();

    // In a production app I would use Dagger to inject the appropriate filter
    private CitiesSearchFilter mFilter = new CitiesSearchFilterIndexMap(new CitiesSorterNameCountry(),
                                            new StringSanitizerImpl());

    @Before
    public void onTestStart() {
        mCities.clear();
        mCities.add(SydneyUS);
        mCities.add(Albuquerque);
        mCities.add(SydneyAU);
        mCities.add(Alabama);
        mCities.add(Arizona);
        mCities.add(Anaheim);
        mFilter.initWithCities(mCities);
    }

    @Test
    public void initWithCities_sortsByNameAndCountry() {
        List<City> result = mFilter.getAll();
        assertEquals(Alabama,       result.get(0));
        assertEquals(Albuquerque,   result.get(1));
        assertEquals(Anaheim,       result.get(2));
        assertEquals(Arizona,       result.get(3));
        assertEquals(SydneyAU,      result.get(4));
        assertEquals(SydneyUS,      result.get(5));
    }

    @Test
    public void filterByPrefix_A_returnsOnlyCitiesStartingWithA() {
        // If the given prefix is "A", all cities but Sydney should appear.
        // Contrariwise, if the given prefix is "s", the only result should be "Sydney, AU".
        String prefix = "A";
        List<City> result = mFilter.filterByPrefix(prefix);
        assertEquals(4, result.size());
        assertFalse(result.contains(SydneyAU));
        assertFalse(result.contains(SydneyUS));
    }

    @Test
    public void filterByPrefix_Al_returnsOnlyCitiesStartingWithAl() {
        // If the given prefix is "Al", "Alabama, US" and "Albuquerque, US" are the only results.
        String prefix = "Al";
        List<City> result = mFilter.filterByPrefix(prefix);
        assertEquals(2, result.size());
        assertTrue(result.contains(Alabama));
        assertTrue(result.contains(Albuquerque));
    }

    @Test
    public void filterByPrefix_Alb_returnsOnlyCitiesStartingWithAlb() {
        // If the prefix given is "Alb" then the only result is "Albuquerque, US"
        String prefix = "Alb";
        List<City> result = mFilter.filterByPrefix(prefix);
        assertEquals(1, result.size());
        assertTrue(result.contains(Albuquerque));
    }

    @Test
    public void filterByPrefix_null_returnsAll() {
        String prefix = null;
        List<City> result = mFilter.filterByPrefix(prefix);
        assertEquals(6, result.size());
    }

    @Test
    public void filterByPrefix_emptyString_returnsAll() {
        String prefix = "";
        List<City> result = mFilter.filterByPrefix(prefix);
        assertEquals(6, result.size());
    }

    @Test
    public void filterByPrefix_unexpectedCharacters_returnsEmptyList() {
        String prefix = "%$œ±文\uD83D\uDE0F";
        List<City> result = mFilter.filterByPrefix(prefix);
        assertTrue(result.isEmpty());
    }

}
