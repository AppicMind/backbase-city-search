package com.appicmind.backbase.citysearch.viewmodel;

import com.appicmind.backbase.citysearch.data.filter.CitiesSearchFilter;
import com.appicmind.backbase.citysearch.data.filter.CitiesSearchFilterIndexMap;
import com.appicmind.backbase.citysearch.data.filter.CitiesSorter;
import com.appicmind.backbase.citysearch.data.filter.CitiesSorterNameCountry;
import com.appicmind.backbase.citysearch.data.filter.StringSanitizer;
import com.appicmind.backbase.citysearch.data.filter.StringSanitizerImpl;
import com.appicmind.backbase.citysearch.data.repository.CitiesRepository;
import com.appicmind.backbase.citysearch.data.source.CitiesAssetSource;

import android.content.res.AssetManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * @author marcocoenradie
 * @since 16-10-2019
 */
public class CitiesViewModelFactory implements ViewModelProvider.Factory {

    private AssetManager mAssetManager;
    private String mCitiesJsonFileName;

    public CitiesViewModelFactory(AssetManager assetManager, String citiesJsonFileName) {
        mAssetManager = assetManager;
        mCitiesJsonFileName = citiesJsonFileName;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        // In a production app I would use Dagger to inject these implementations
        CitiesAssetSource assetSource = new CitiesAssetSource(mAssetManager, mCitiesJsonFileName);
        CitiesSorter citiesSorter = new CitiesSorterNameCountry();
        StringSanitizer sanitizer = new StringSanitizerImpl();
        CitiesSearchFilter searchFilter = new CitiesSearchFilterIndexMap(citiesSorter, sanitizer);
        CitiesRepository repository = new CitiesRepository(assetSource, searchFilter);
        return (T) new CitiesViewModel(repository);
    }

}
