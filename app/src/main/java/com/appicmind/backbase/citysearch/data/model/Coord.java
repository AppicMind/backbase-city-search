package com.appicmind.backbase.citysearch.data.model;

import java.io.Serializable;

/**
 * @author marcocoenradie
 * @since 15-10-2019
 */
public class Coord implements Serializable {

    private double lat;
    private double lon;

    public Coord() {}

    public Coord(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
