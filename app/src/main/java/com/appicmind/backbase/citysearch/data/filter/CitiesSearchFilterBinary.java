package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Filter which can filter a List of {@link City}'s by start prefix
 *
 * The {@link #filter(String)} method calls {@link #findFirstIndexForPrefix(List, String)} to
 * find the index of the first matching item using a binary search and then loops over the following
 * items to build until all matching items have been found.
 *
 * @author marcocoenradie
 * @since 17-10-2019
 */
@Deprecated
public class CitiesSearchFilterBinary implements CitiesSearchFilter {

    private CitiesSorter mCitiesSorter;
    private StringSanitizer mStringSanitizer;

    private List<City> mAllCities;

    public CitiesSearchFilterBinary(CitiesSorter citiesSorter, StringSanitizer sanitizer) {
        mCitiesSorter = citiesSorter;
        mStringSanitizer = sanitizer;
    }

    @Override
    public void initWithCities(@NonNull List<City> cities) {
        mAllCities = mCitiesSorter.sortCities(cities);
    }

    @Nullable
    @Override
    public List<City> getAll() {
        return mAllCities;
    }

    @NonNull
    @Override
    public List<City> filterByPrefix(@NonNull String prefix) {
        long startTime = System.nanoTime();
        List<City> result = filter(prefix);
        long endTime = System.nanoTime();
        long elapsedNano = endTime - startTime;
        System.out.println("filterByPrefix time: " + elapsedNano + " for prefix " + prefix);
        return result;
    }

    private List<City> filter(String prefix) {
        List<City> all = getAll();
        if (all == null) {
            return Collections.emptyList();
        }

        prefix = mStringSanitizer.sanitize(prefix);
        if (prefix.isEmpty()) {
            return all;
        }

        int firstIndex = findFirstIndexForPrefix(all, prefix);
        if (firstIndex < 0) {
            return Collections.emptyList();
        }
        return getSubsetForPrefixStartingAtIndex(all, prefix, firstIndex);
    }

    private int findFirstIndexForPrefix(List<City> cities, String prefix) {
        int firstIndex = 0;
        int lastIndex = cities.size() - 1;
        int result = -1;

        while(firstIndex <= lastIndex) {
            int middleIndex = (firstIndex + lastIndex) / 2;
            String cityName = mStringSanitizer.sanitize(cities.get(middleIndex).getName());
            // if the middle element equals the search prefix
            if (cityName.equals(prefix)) {
                // check if previous items are also identical (for example other country)
                while(middleIndex > 1 && mStringSanitizer.sanitize(cities.get(middleIndex - 1).getName()).equals(prefix)) {
                    middleIndex--;
                }
                // return the first matching index
                return middleIndex;
            }

            // if the middle element starts with our prefix
            // store index of this matching element
            // and point our index to the middle-1, taking the second half of the list out of consideration
            else if (cityName.startsWith(prefix)) {
                result = middleIndex;
                lastIndex = middleIndex - 1;
            }

            // if the middle element is smaller
            // point our index to the middle+1, taking the first half out of consideration
            else if (cityName.compareTo(prefix) < 0) {
                firstIndex = middleIndex + 1;
            }

            // if the middle element is bigger
            // point our index to the middle-1, taking the second half out of consideration
            else if (cityName.compareTo(prefix) > 0) {
                lastIndex = middleIndex - 1;
            }
        }
        return result;
    }

    private List<City> getSubsetForPrefixStartingAtIndex(List<City> cities, String prefix, int fromIndex) {
        List<City> result = new ArrayList();
        List<City> subList = cities.subList(fromIndex, cities.size());
        for (City city : subList) {
            if (mStringSanitizer.sanitize(city.getName()).startsWith(prefix)) {
                result.add(city);
            } else {
                break;
            }
        }
        return result;
    }
}
