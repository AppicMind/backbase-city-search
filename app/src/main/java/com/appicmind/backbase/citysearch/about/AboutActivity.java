package com.appicmind.backbase.citysearch.about;


import com.appicmind.backbase.citysearch.R;
import com.appicmind.backbase.citysearch.data.model.City;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AboutActivity extends AppCompatActivity implements About.View {

    private TextView cityName;
    private TextView country;
    private TextView coordinates;
    private ProgressBar progressBar;
    private android.view.View errorView;
    private android.view.View infoContainer;

    private AboutProgressListener progressListener;

    public void setProgressListener(
            AboutProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    public static final Intent getIntentForCity(Context context, City city) {
        Intent intent = new Intent(context, AboutActivity.class);
        intent.putExtras(AboutPresenterImpl.getExtrasBundleForCity(city));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AboutPresenterImpl aboutPresenter = new AboutPresenterImpl(this, getIntent().getExtras());
        cityName = findViewById(R.id.city_name);
        country = findViewById(R.id.country);
        coordinates = findViewById(R.id.coordinates);
        progressBar = findViewById(R.id.progressBar);
        errorView = findViewById(R.id.errorView);
        infoContainer = findViewById(R.id.infoContainer);
        aboutPresenter.getCityInfo();
    }

    @Override
    public void setCityName(String cityName) {
        infoContainer.setVisibility(android.view.View.VISIBLE);
        this.cityName.setText(cityName);
    }

    @Override
    public void setCountry(String country) {
        this.country.setText(country);
    }

    @Override
    public void setCoordinates(String coordinates) {
        this.coordinates.setText(coordinates);
    }

    @Override
    public void showError() {
        errorView.setVisibility(android.view.View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(android.view.View.VISIBLE);
        if (progressListener != null) {
            progressListener.onProgressChanged(true);
        }
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(android.view.View.GONE);
        if (progressListener != null) {
            progressListener.onProgressChanged(false);
        }
    }

    public interface AboutProgressListener {
        void onProgressChanged(boolean inProgress);
    }
}
