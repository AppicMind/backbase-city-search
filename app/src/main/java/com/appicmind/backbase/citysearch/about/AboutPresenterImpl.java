package com.appicmind.backbase.citysearch.about;


import com.appicmind.backbase.citysearch.data.model.City;

import android.os.Bundle;
import android.os.Handler;

import java.lang.ref.WeakReference;
import java.util.Locale;

/**
 * Created by Backbase R&D B.V on 28/06/2018.
 */

public class AboutPresenterImpl implements About.Presenter {

    private final WeakReference<About.View> aboutView;
    private final AboutModelImpl aboutModel;

    public static final Bundle getExtrasBundleForCity(City city) {
        return AboutModelImpl.getExtrasBundleForCity(city);
    }

    public AboutPresenterImpl(About.View view, Bundle extras){
        this.aboutView = new WeakReference<>(view);
        this.aboutModel = new AboutModelImpl(this, extras);
    }

    @Override
    public void getCityInfo() {
        About.View aboutViewImpl = aboutView.get();

        aboutViewImpl.showProgress();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                aboutModel.getCityInfo();
            }
        }, 1000);
    }

    @Override
    public void onSuccess(City city) {
        About.View aboutViewImpl = aboutView.get();

        if(aboutViewImpl != null){
            aboutViewImpl.hideProgress();
            aboutViewImpl.setCityName(city.getName());
            aboutViewImpl.setCountry(city.getCountry());
            aboutViewImpl.setCoordinates(String.format(Locale.US, "%.6f, %.6f",
                    city.getCoord().getLat(), city.getCoord().getLon()));
        }

    }

    @Override
    public void onFail() {
        About.View aboutViewImpl = aboutView.get();
        if (aboutViewImpl != null){
            aboutViewImpl.hideProgress();
            aboutViewImpl.showError();
        }
    }
}
