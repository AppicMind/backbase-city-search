package com.appicmind.backbase.citysearch.data.model;

import java.io.Serializable;

/**
 * @author marcocoenradie
 * @since 15-10-2019
 */
public class City implements Serializable {
    private long _id;
    private String name;
    private String country;
    private Coord coord;

    public City() {}

    public City(String name, String country) {
        this(name, country, new Coord(0, 0));
    }

    public City(String name, String country, Coord coord) {
        this.name = name;
        this.country = country;
        this.coord = coord;
    }

    public long getId() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public Coord getCoord() {
        return coord;
    }
}
