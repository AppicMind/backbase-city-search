package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author marcocoenradie
 * @since 20-10-2019
 */
public class CitiesSorterNameCountry implements CitiesSorter {

    @Override
    public List<City> sortCities(List<City> cities) {
        Collections.sort(cities, new Comparator<City>(){
            public int compare(City city1, City city2) {
                int comparison = city1.getName().compareToIgnoreCase(city2.getName());
                if (comparison == 0) {
                    comparison = city1.getCountry().compareToIgnoreCase(city2.getCountry());
                }
                return comparison;
            }
        });
        return cities;
    }
}
