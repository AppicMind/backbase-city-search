package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Filter which can filter a List of {@link City}'s by start prefix
 *
 * The {@link #filter(String)} method calls {@link #findFirstIndexForPrefix(List, String)} to
 * find the index of the first matching item using a binary search and then loops over the following
 * items find the index of the last matching element. The indices are then stored in a Map
 * for future reference.
 *
 *
 * @author marcocoenradie
 * @since 17-10-2019
 */
public class CitiesSearchFilterIndexMap implements CitiesSearchFilter {

    private CitiesSorter mCitiesSorter;
    private StringSanitizer mStringSanitizer;

    private List<City> mAllCities;
    /**
     * A Map to store the first and last index of the matching City elements for each prefix that
     * the user enters.
     */
    private final Map<String, int[]> indexMap = new HashMap<>();

    public CitiesSearchFilterIndexMap(CitiesSorter citiesSorter, StringSanitizer sanitizer) {
        mCitiesSorter = citiesSorter;
        mStringSanitizer = sanitizer;
    }

    @Override
    public void initWithCities(@NonNull List<City> cities) {
        mAllCities = mCitiesSorter.sortCities(cities);
        indexMap.clear();
    }

    @Nullable
    @Override
    public List<City> getAll() {
        return mAllCities;
    }

    @NonNull
    @Override
    public List<City> filterByPrefix(@NonNull String prefix) {
        long startTime = System.nanoTime();
        List<City> result = filter(prefix);
        long endTime = System.nanoTime();
        long elapsedNano = endTime - startTime;
        System.out.println("filterByPrefix time: " + elapsedNano + " for prefix " + prefix);
        return result;
    }

    private List<City> filter(String prefix) {
        List<City> all = getAll();
        if (all == null) {
            return Collections.emptyList();
        }

        prefix = mStringSanitizer.sanitize(prefix);
        if (prefix.isEmpty()) {
            return all;
        }

        int[] firstLastIndexPair = indexMap.get(prefix);
        if (firstLastIndexPair == null) {
            firstLastIndexPair = putIndicesForPrefix(prefix);
        }
        if (firstLastIndexPair[0] == -1) {
            return Collections.emptyList();
        }
        return mAllCities.subList(firstLastIndexPair[0], firstLastIndexPair[1] + 1);
    }

    private int[] putIndicesForPrefix(String prefix) {
        int firstIndex = findFirstIndexForPrefix(mAllCities, prefix);
        int lastIndex = -1;
        if (firstIndex > -1) {
            lastIndex = findLastIndexForPrefixStartingAtIndex(mAllCities, prefix, firstIndex);
        }
        int[] firstLastIndexPair = new int[]{firstIndex, lastIndex};
        indexMap.put(prefix, firstLastIndexPair);
        return firstLastIndexPair;
    }

    private int findFirstIndexForPrefix(List<City> cities, String prefix) {
        int firstIndex = 0;
        int lastIndex = cities.size() - 1;
        int result = -1;

        while(firstIndex <= lastIndex) {
            int middleIndex = (firstIndex + lastIndex) / 2;
            String cityName = mStringSanitizer.sanitize(cities.get(middleIndex).getName());
            // if the middle element equals the search prefix
            if (cityName.equals(prefix)) {
                // check if previous items are also identical (for example other country)
                while(middleIndex > 1 && mStringSanitizer.sanitize(cities.get(middleIndex - 1).getName()).equals(prefix)) {
                    middleIndex--;
                }
                // return the first matching index
                return middleIndex;
            }

            // if the middle element starts with our prefix
            // store index of this matching element
            // and point our index to the middle-1, taking the second half of the list out of consideration
            else if (cityName.startsWith(prefix)) {
                result = middleIndex;
                lastIndex = middleIndex - 1;
            }

            // if the middle element is smaller
            // point our index to the middle+1, taking the first half out of consideration
            else if (cityName.compareTo(prefix) < 0) {
                firstIndex = middleIndex + 1;
            }

            // if the middle element is bigger
            // point our index to the middle-1, taking the second half out of consideration
            else if (cityName.compareTo(prefix) > 0) {
                lastIndex = middleIndex - 1;
            }
        }
        return result;
    }

    private int findLastIndexForPrefixStartingAtIndex(List<City> cities, String prefix, int fromIndex) {
        int numMatches = 0;
        List<City> subList = cities.subList(fromIndex, cities.size());
        for (City city : subList) {
            if (mStringSanitizer.sanitize(city.getName()).startsWith(prefix)) {
                numMatches++;
            } else {
                break;
            }
        }
        return fromIndex + (numMatches - 1);
    }
}
