package com.appicmind.backbase.citysearch.data.repository;

import com.appicmind.backbase.citysearch.data.filter.CitiesSearchFilter;
import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.data.source.CitiesAssetSource;

import android.os.Handler;
import android.os.HandlerThread;

import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class CitiesRepository {

    private CitiesAssetSource mAssetSource;
    private CitiesSearchFilter mCitiesSearchFilter;
    private MutableLiveData<List<City>> mCitiesData = new MutableLiveData<>();

    private HandlerThread handlerThread;
    private Handler handler;

    public CitiesRepository(CitiesAssetSource assetSource, CitiesSearchFilter searchFilter) {
        mAssetSource = assetSource;
        mCitiesSearchFilter = searchFilter;
    }

    public LiveData<List<City>> getCitiesData() {
        return mCitiesData;
    }

    public void fetchAllCities() {
        if (mCitiesSearchFilter.getAll() != null) {
            mCitiesData.setValue(mCitiesSearchFilter.getAll());
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                try {
                    mCitiesSearchFilter.initWithCities(mAssetSource.getCities());
                    mCitiesData.postValue(mCitiesSearchFilter.getAll());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void filterCities(@NonNull final String prefix) {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
        prepareHandlerThreadAndHandler();
        handler.post(new Runnable() {
            public void run() {
                List<City> filteredCities = mCitiesSearchFilter.filterByPrefix(prefix);
                mCitiesData.postValue(filteredCities);
            }
        });
    }

    private void prepareHandlerThreadAndHandler() {
        if (handlerThread == null || !handlerThread.isAlive()) {
            createHandlerThread();
            createHandler();
        } else if (handler == null) {
            createHandler();
        }
    }

    private void createHandlerThread() {
        handlerThread = new HandlerThread("tag", HandlerThread.MIN_PRIORITY);
        handlerThread.start();
    }

    private void createHandler() {
        handler = new Handler(handlerThread.getLooper());
    }
}
