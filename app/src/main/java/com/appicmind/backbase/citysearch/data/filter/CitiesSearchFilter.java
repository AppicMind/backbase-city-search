package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author marcocoenradie
 * @since 17-10-2019
 */
public interface CitiesSearchFilter {

    void initWithCities(@NonNull List<City> cities);
    @Nullable List<City> getAll();
    @NonNull List<City> filterByPrefix(@NonNull String prefix);

}
