package com.appicmind.backbase.citysearch.presentation.view;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.appicmind.backbase.citysearch.R;
import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.viewmodel.CitiesViewModel;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class CityMapFragment extends Fragment {

    private City mCity;
    private CitiesViewModel mViewModel;
    private GoogleMap mGoogleMap;

    private Observer<City> mSelectedCityObserver = new Observer<City>() {
        @Override
        public void onChanged(City city) {
            mCity = city;
            updateUI();
        }
    };

    private OnMapReadyCallback mOnMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mGoogleMap = googleMap;
            updateUI();
        }
    };

    private void updateUI() {
        // Show the dummy content as text in a TextView.
        if (mCity != null && mGoogleMap != null) {
            // Add a marker and move the camera
            LatLng latLng = new LatLng(mCity.getCoord().getLat(), mCity.getCoord().getLon());
            mGoogleMap.clear();
            Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(
                    mCity.getName()));
            marker.showInfoWindow();
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CityMapFragment() { }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mViewModel = ViewModelProviders.of(getActivity()).get(CitiesViewModel.class);
        mViewModel.observeSelectedCity(this, mSelectedCityObserver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_city_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(mOnMapReadyCallback);
    }
}
