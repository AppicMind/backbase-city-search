package com.appicmind.backbase.citysearch.data.filter;

import java.util.Locale;

/**
 * @author marcocoenradie
 * @since 20-10-2019
 */
public class StringSanitizerImpl implements StringSanitizer {

    public String sanitize(String string) {
        if (string == null) {
            return "";
        }
        return string.trim().toLowerCase(Locale.US);
    }

}
