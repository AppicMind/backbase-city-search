package com.appicmind.backbase.citysearch.presentation.adapter;

import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.presentation.adapter.viewholder.CityViewHolderBinder;
import com.appicmind.backbase.citysearch.presentation.adapter.viewholder.ViewType;

import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author marcocoenradie
 * @since 16-10-2019
 */
public class CitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private CityViewHolderBinder mCityViewHolderBinder;

    private final List<City> mCities = new ArrayList<>();
    private City mSelectedCity;

    public CitiesAdapter(CityViewHolderBinder cityViewHolderBinder) {
        mCityViewHolderBinder = cityViewHolderBinder;

        setHasStableIds(true);
    }

    public void setCities(List<City> cities) {
        mCities.clear();
        mCities.addAll(cities);
    }

    public void setSelectedCity(City selectedCity) {
        mSelectedCity = selectedCity;
    }

    public City getSelectedCity() {
        return mSelectedCity;
    }

    public int getSelectedCityAdapterPosition() {
        if (mCities == null || mSelectedCity == null) {
            return -1;
        }
        return mCities.indexOf(mSelectedCity);
    }

    private City getItem(int position) {
        return mCities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return ViewType.CITY.value;
    }

    @Override
    public int getItemCount() {
        return mCities.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return mCityViewHolderBinder.createViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CityViewHolderBinder.CityViewHolder) {
            City city = getItem(position);
            boolean selected = mSelectedCity != null && city.getId() == mSelectedCity.getId();
            mCityViewHolderBinder.bindViewHolder((CityViewHolderBinder.CityViewHolder)holder,
                    city, selected);
        }
    }

    public void setOnCityClickListener(CityViewHolderBinder.OnCityClickListener onCityClickListener) {
        mCityViewHolderBinder.setOnCityClickListener(onCityClickListener);
    }
}
