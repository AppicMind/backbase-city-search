package com.appicmind.backbase.citysearch.about;

import com.appicmind.backbase.citysearch.data.model.City;

import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * Created by Backbase R&D B.V on 28/06/2018.
 */

public class AboutModelImpl implements About.Model {

    private static final String EXTRA_KEY_CITY = "city";
    private final About.Presenter presenter;
    private final Bundle extras;

    public static final Bundle getExtrasBundleForCity(City city) {
        Bundle extras = new Bundle();
        extras.putSerializable(EXTRA_KEY_CITY, city);
        return extras;
    }

    public AboutModelImpl(@NonNull About.Presenter presenter, Bundle extras){
        this.presenter = presenter;
        this.extras = extras;
    }

    @Override
    public void getCityInfo() {
        City city = extras == null ? null : (City) extras.getSerializable(EXTRA_KEY_CITY);

        if (city == null || city.getName() == null || city.getCountry() == null || city.getCoord() == null) {
            presenter.onFail();
            return;
        }

        presenter.onSuccess(city);
    }
}
