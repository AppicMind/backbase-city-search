package com.appicmind.backbase.citysearch.viewmodel;

import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.data.repository.CitiesRepository;

import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

/**
 * @author marcocoenradie
 * @since 15-10-2019
 */
public class CitiesViewModel extends ViewModel {

    private CitiesRepository mCitiesRepository;
    private boolean mHasStartedFetchingCities = false;
    private MediatorLiveData<List<City>> mCitiesData = new MediatorLiveData<>();
    private MutableLiveData<City> mSelectedCityData = new MutableLiveData<>();
    private MutableLiveData<City> mCityInfoClickedData = new MutableLiveData<>();

    public CitiesViewModel(CitiesRepository citiesRepository) {
        mCitiesRepository = citiesRepository;
        mCitiesData.addSource(mCitiesRepository.getCitiesData(), new Observer<List<City>>() {
            @Override
            public void onChanged(List<City> cities) {
                mCitiesData.setValue(cities);
            }
        });
    }

    public void observeCities(LifecycleOwner lifecycleOwner, Observer<List<City>> observer) {
        mCitiesData.observe(lifecycleOwner, observer);
        if (mCitiesData.getValue() == null && !mHasStartedFetchingCities) {
            mHasStartedFetchingCities = true;
            mCitiesRepository.fetchAllCities();
        }
    }

    public void observeSelectedCity(LifecycleOwner lifecycleOwner, Observer<City> observer) {
        mSelectedCityData.observe(lifecycleOwner, observer);
    }

    public void observeCityInfoClicked(LifecycleOwner lifecycleOwner, final Observer<City> observer) {
        mCityInfoClickedData.observe(lifecycleOwner, observer);
    }

    public void onCityClick(City city) {
        mSelectedCityData.setValue(city);
    }

    public void onCityInfoClick(City city) {
        mCityInfoClickedData.setValue(city);
    }

    public void onFilterQueryTextChange(String prefix) {
        mCitiesRepository.filterCities(prefix);
    }
}
