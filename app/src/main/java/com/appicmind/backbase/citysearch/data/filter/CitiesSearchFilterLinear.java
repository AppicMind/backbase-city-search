package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Filter which can filter a List of {@link City}'s by start prefix
 *
 * The {@link #filter(String)} method loops over the given Cities linearly until the matching items
 * have been found.
 *
 * @author marcocoenradie
 * @since 17-10-2019
 */
@Deprecated
public class CitiesSearchFilterLinear implements CitiesSearchFilter {

    private CitiesSorter mCitiesSorter;
    private StringSanitizer mStringSanitizer;

    private List<City> mAllCities;

    public CitiesSearchFilterLinear(CitiesSorter citiesSorter, StringSanitizer sanitizer) {
        mCitiesSorter = citiesSorter;
        mStringSanitizer = sanitizer;
    }


    @Override
    public void initWithCities(@NonNull List<City> cities) {
        mAllCities = mCitiesSorter.sortCities(cities);
    }

    @Nullable
    @Override
    public List<City> getAll() {
        return mAllCities;
    }

    @NonNull
    @Override
    public List<City> filterByPrefix(@NonNull String prefix) {
        long startTime = System.nanoTime();
        List<City> result = filter(prefix);
        long endTime = System.nanoTime();
        long elapsedNano = endTime - startTime;
        System.out.println("filterByPrefix time: " + elapsedNano + " for prefix " + prefix);
        return result;
    }

    @NonNull
    private List<City> filter(@NonNull String prefix) {
        List<City> all = getAll();
        if (all == null) {
            return Collections.emptyList();
        }

        prefix = mStringSanitizer.sanitize(prefix);
        if (prefix.isEmpty()) {
            return all;
        }

        List<City> result = new ArrayList();
        for (City city : all) {
            if (mStringSanitizer.sanitize(city.getName()).startsWith(prefix)) {
                result.add(city);
            } else if (result.size() > 0) {
                break;
            }
        }
        return result;
    }


}
