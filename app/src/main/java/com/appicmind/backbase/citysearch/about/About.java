package com.appicmind.backbase.citysearch.about;

import com.appicmind.backbase.citysearch.data.model.City;

/**
 * Created by Backbase R&D B.V on 28/06/2018.
 * MVP contract for AboutActivity
 */

public interface About {

    interface Model {
        void getCityInfo();
    }

    interface Presenter {
        void getCityInfo();
        void onSuccess(City city);
        void onFail();
    }

    interface View {
        void setCityName(String cityName);
        void setCountry(String country);
        void setCoordinates(String coordinates);
        void showError();
        void showProgress();
        void hideProgress();
    }
}
