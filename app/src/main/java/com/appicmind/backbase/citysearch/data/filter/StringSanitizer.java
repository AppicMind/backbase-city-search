package com.appicmind.backbase.citysearch.data.filter;

/**
 * @author marcocoenradie
 * @since 20-10-2019
 */
public interface StringSanitizer {

    String sanitize(String string);

}
