package com.appicmind.backbase.citysearch.data.source;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import com.appicmind.backbase.citysearch.data.model.City;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * @author marcocoenradie
 * @since 16-10-2019
 */
public class CitiesAssetSource {

    private AssetManager mAssetManager;
    private String mCitiesJsonFileName;

    public CitiesAssetSource(AssetManager assetManager, String citiesJsonFileName) {
        mAssetManager = assetManager;
        mCitiesJsonFileName = citiesJsonFileName;
    }

    public List<City> getCities() throws IOException {
        InputStream is = mAssetManager.open(mCitiesJsonFileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        JsonReader reader = new JsonReader(br);
        City[] cityArray = new Gson().fromJson(reader, City[].class);
        return  Arrays.asList(cityArray);
    }
}
