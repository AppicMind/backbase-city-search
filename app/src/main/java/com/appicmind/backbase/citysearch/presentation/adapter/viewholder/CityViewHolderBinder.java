package com.appicmind.backbase.citysearch.presentation.adapter.viewholder;

import com.appicmind.backbase.citysearch.R;
import com.appicmind.backbase.citysearch.data.model.City;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * @author marcocoenradie
 * @since 16-10-2019
 */
public class CityViewHolderBinder {

    private OnCityClickListener mOnCityClickListener;

    public CityViewHolder createViewHolder(ViewGroup parent) {
        CityViewHolder holder = new CityViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_city, parent, false));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCityClickListener != null) {
                    mOnCityClickListener.onCityClick((City)v.getTag());
                }
            }
        });

        holder.infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCityClickListener != null) {
                    mOnCityClickListener.onCityInfoClick((City)v.getTag());
                }
            }
        });

        return holder;
    }

    public void setOnCityClickListener(
            OnCityClickListener onCityClickListener) {
        mOnCityClickListener = onCityClickListener;
    }

    public void bindViewHolder(CityViewHolder holder, City city, boolean selected) {
        holder.itemView.setTag(city);
        holder.infoButton.setTag(city);
        holder.cityItemContent.setBackgroundColor(selected ? Color.LTGRAY : Color.TRANSPARENT);
        holder.cityNameView.setText(
                String.format(Locale.US, "%s, %s", city.getName(), city.getCountry()));
        holder.cityCoordinatesView.setText(
                String.format(Locale.US, "%.6f, %.6f",
                        city.getCoord().getLat(), city.getCoord().getLon()));
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {
        View cityItemContent, infoButton;
        TextView cityNameView, cityCoordinatesView;

        private CityViewHolder(@NonNull View itemView) {
            super(itemView);
            cityItemContent = itemView.findViewById(R.id.city_item_content);
            infoButton = itemView.findViewById(R.id.city_info_btn);
            cityNameView = itemView.findViewById(R.id.city_label);
            cityCoordinatesView = itemView.findViewById(R.id.city_coordinates);
        }
    }

    public interface OnCityClickListener {
        void onCityClick(City city);
        void onCityInfoClick(City city);
    }
}
