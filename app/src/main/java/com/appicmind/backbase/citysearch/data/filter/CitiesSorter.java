package com.appicmind.backbase.citysearch.data.filter;

import com.appicmind.backbase.citysearch.data.model.City;

import java.util.List;

/**
 * @author marcocoenradie
 * @since 20-10-2019
 */
public interface CitiesSorter {

    List<City> sortCities(List<City> cities);

}
