package com.appicmind.backbase.citysearch.presentation.view;

import com.appicmind.backbase.citysearch.R;
import com.appicmind.backbase.citysearch.about.AboutActivity;
import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.viewmodel.CitiesViewModel;
import com.appicmind.backbase.citysearch.viewmodel.CitiesViewModelFactory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class CitiesActivity extends AppCompatActivity {

    private static final String STATE_KEY_SELECTED_CITY = "selected_city";
    private static final String STATE_KEY_CLICKED_CITY_INFO = "clicked_city_info";
    private @NonNull Bundle mSavedInstanceState = new Bundle();
    private long mSelectedCityId = -1;
    private long mClickedCityInfoId = -1;

    private boolean mTwoPane;
    private CityMapFragment mCityMapFragment;

    private Observer<City> mSelectedCityObserver = new Observer<City>() {
        @Override
        public void onChanged(City city) {
            mSelectedCityId = city.getId();
            if (mTwoPane || mSavedInstanceState.containsKey(STATE_KEY_SELECTED_CITY) || mCityMapFragment == null) {
                // don't show map if in two pane map (it is already shown) or after orientation change
                mSavedInstanceState.remove(STATE_KEY_SELECTED_CITY);
                return;
            }

            // show map fragment
            getSupportFragmentManager().beginTransaction()
                    .show(mCityMapFragment)
                    .addToBackStack(null)
                    .commit();

            // adjust action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(city.getName());

            // hide the keyboard
            View focusedView = getCurrentFocus();
            if (focusedView != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
            }
        }
    };

    private Observer<City> mCityInfoClickObserver = new Observer<City>() {
        @Override
        public void onChanged(City city) {
            mClickedCityInfoId = city.getId();
            if (mSavedInstanceState.containsKey(STATE_KEY_CLICKED_CITY_INFO)) {
                // don't start AboutActivity again after orientation change
                mSavedInstanceState.remove(STATE_KEY_CLICKED_CITY_INFO);
                return;
            }
            Intent intent = AboutActivity.getIntentForCity(CitiesActivity.this, city);
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSavedInstanceState = savedInstanceState == null ? new Bundle() : savedInstanceState;

        CitiesViewModelFactory factory = new CitiesViewModelFactory(getAssets(), "cities.json");
        CitiesViewModel viewModel = ViewModelProviders.of(this, factory).get(CitiesViewModel.class);
        viewModel.observeSelectedCity(this, mSelectedCityObserver);
        viewModel.observeCityInfoClicked(this, mCityInfoClickObserver);

        setContentView(R.layout.activity_cities);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        mTwoPane = ((ViewGroup)findViewById(R.id.cities_content) instanceof LinearLayout);
        mCityMapFragment = (CityMapFragment) getSupportFragmentManager().findFragmentById(R.id.city_map);
        if (mCityMapFragment != null) {
            FragmentTransaction t = getSupportFragmentManager().beginTransaction();
            if (mTwoPane) {
                t.show(mCityMapFragment);
            } else {
                t.hide(mCityMapFragment);
            }
            t.commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mSelectedCityId > -1) {
            outState.putLong(STATE_KEY_SELECTED_CITY, mSelectedCityId);
        }
        if (mClickedCityInfoId > -1) {
            outState.putLong(STATE_KEY_CLICKED_CITY_INFO, mClickedCityInfoId);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mTwoPane) {
            finish();
            return;
        }
        getSupportActionBar().setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        super.onBackPressed();
    }
}
