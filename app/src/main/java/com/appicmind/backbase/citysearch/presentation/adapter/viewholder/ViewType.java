package com.appicmind.backbase.citysearch.presentation.adapter.viewholder;

/**
 * @author marcocoenradie
 * @since 16-10-2019
 */
public enum ViewType {
    CITY(0);

    public int value;
    ViewType(int value) {
        this.value = value;
    }
}
