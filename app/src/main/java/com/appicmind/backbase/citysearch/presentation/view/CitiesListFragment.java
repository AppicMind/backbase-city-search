package com.appicmind.backbase.citysearch.presentation.view;

import com.appicmind.backbase.citysearch.R;
import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.presentation.adapter.CitiesAdapter;
import com.appicmind.backbase.citysearch.presentation.adapter.viewholder.CityViewHolderBinder;
import com.appicmind.backbase.citysearch.viewmodel.CitiesViewModel;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

public class CitiesListFragment extends Fragment {

    private SearchView mSearchView;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private CitiesAdapter mCitiesAdapter;
    private CitiesViewModel mViewModel;

    private Observer<List<City>> mCitiesObserver = new Observer<List<City>>() {
        @Override
        public void onChanged(List<City> cities) {
            mCitiesAdapter.setCities(cities);
            mCitiesAdapter.notifyDataSetChanged();

            if (!cities.isEmpty()) {
                setUILoadingState(false);
            }
        }
    };

    private Observer<City> mSelectedCityObserver = new Observer<City>() {
        @Override
        public void onChanged(City city) {
            mCitiesAdapter.setSelectedCity(city);
            mCitiesAdapter.notifyDataSetChanged();
            int selectedPosition = mCitiesAdapter.getSelectedCityAdapterPosition();
            if (selectedPosition > -1) {
                mRecyclerView.smoothScrollToPosition(selectedPosition);
            }
        }
    };

    private final CityViewHolderBinder.OnCityClickListener mOnCityClickListener = new CityViewHolderBinder.OnCityClickListener() {
        @Override
        public void onCityClick(City city) {
            mViewModel.onCityClick(city);
        }

        @Override
        public void onCityInfoClick(City city) {
            mViewModel.onCityInfoClick(city);
        }
    };

    private SearchView.OnQueryTextListener mOnQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String newText) {
            // your text view here
            mViewModel.onFilterQueryTextChange(newText);
            return true;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CitiesListFragment() { }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCitiesAdapter = new CitiesAdapter(new CityViewHolderBinder());
        mCitiesAdapter.setOnCityClickListener(mOnCityClickListener);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mViewModel = ViewModelProviders.of(getActivity()).get(CitiesViewModel.class);
        mViewModel.observeCities(this, mCitiesObserver);
        mViewModel.observeSelectedCity(this, mSelectedCityObserver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cities_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSearchView = view.findViewById(R.id.searchView);
        mSearchView.setOnQueryTextListener(mOnQueryTextListener);

        mRecyclerView = view.findViewById(R.id.item_list);
        mRecyclerView.setAdapter(mCitiesAdapter);

        mProgressBar = view.findViewById(R.id.progress);
    }

    private void setUILoadingState(boolean loading) {
        mSearchView.setVisibility(loading ? View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility(loading ? View.GONE : View.VISIBLE);
        mProgressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
