package com.appicmind.backbase.citysearch.about;

import com.appicmind.backbase.citysearch.R;
import com.appicmind.backbase.citysearch.data.model.City;
import com.appicmind.backbase.citysearch.data.model.Coord;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.content.Context;
import android.content.Intent;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.idling.CountingIdlingResource;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;


@RunWith(AndroidJUnit4.class)
public class AboutActivityTest {

    private CountingIdlingResource countingResource = new CountingIdlingResource("AboutProgress");
    private AboutActivity.AboutProgressListener mProgressListener = new AboutActivity.AboutProgressListener() {
        @Override
        public void onProgressChanged(boolean inProgress) {
            if (inProgress) {
                countingResource.increment();
            } else {
                countingResource.decrement();
            }
        }
    };

    @Rule
    public ActivityTestRule<AboutActivity> activityRule
            = new ActivityTestRule<>(AboutActivity.class, true, false);

    @Test
    public void launchedWithValidCity_displaysCorrectCityInfo() {
        City city = new City("Teststad", "Testland",
                new Coord(123.456789, 98.765));

        IdlingRegistry.getInstance().register(countingResource);
        countingResource.increment();

        Context targetContext = InstrumentationRegistry.getInstrumentation()
                .getTargetContext();
        activityRule.launchActivity(AboutActivity.getIntentForCity(targetContext, city));

        activityRule.getActivity().setProgressListener(mProgressListener);

        onView(withId(R.id.infoContainer)).check(matches(isDisplayed()));
        onView(withId(R.id.city_name)).check(matches(withText("Teststad")));
        onView(withId(R.id.country)).check(matches(withText("Testland")));
        onView(withId(R.id.coordinates)).check(matches(withText("123.456789, 98.765000")));
        onView(withId(R.id.errorView)).check(matches(not(isDisplayed())));

        // Clean up
        IdlingRegistry.getInstance().unregister(countingResource);
    }

    @Test
    public void launchedWithIncompleteCity_displaysFailedMessage() {
        City city = new City("Teststad", "Testland", null);

        IdlingRegistry.getInstance().register(countingResource);
        countingResource.increment();

        Context targetContext = InstrumentationRegistry.getInstrumentation()
                .getTargetContext();
        activityRule.launchActivity(AboutActivity.getIntentForCity(targetContext, city));

        activityRule.getActivity().setProgressListener(mProgressListener);

        onView(withId(R.id.errorView)).check(matches(isDisplayed()));
        onView(withId(R.id.infoContainer)).check(matches(not(isDisplayed())));

        // Clean up
        IdlingRegistry.getInstance().unregister(countingResource);
    }

    @Test
    public void launchedWithNullCity_displaysFailedMessage() {
        City city = null;

        IdlingRegistry.getInstance().register(countingResource);
        countingResource.increment();

        Context targetContext = InstrumentationRegistry.getInstrumentation()
                .getTargetContext();
        activityRule.launchActivity(AboutActivity.getIntentForCity(targetContext, city));

        activityRule.getActivity().setProgressListener(mProgressListener);

        onView(withId(R.id.errorView)).check(matches(isDisplayed()));
        onView(withId(R.id.infoContainer)).check(matches(not(isDisplayed())));

        // Clean up
        IdlingRegistry.getInstance().unregister(countingResource);
    }

    @Test
    public void launchedWithIncorrectBundle_displaysFailedMessage() {
        City city = new City("Teststad", "Testland", null);

        IdlingRegistry.getInstance().register(countingResource);
        countingResource.increment();

        Context targetContext = InstrumentationRegistry.getInstrumentation()
                .getTargetContext();
        Intent intent = new Intent(targetContext, AboutActivity.class);
        intent.putExtra("key", "value");
        activityRule.launchActivity(intent);

        activityRule.getActivity().setProgressListener(mProgressListener);

        onView(withId(R.id.errorView)).check(matches(isDisplayed()));
        onView(withId(R.id.infoContainer)).check(matches(not(isDisplayed())));

        // Clean up
        IdlingRegistry.getInstance().unregister(countingResource);
    }
}
